//DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                   Version 2, December 2004
 
//Copyright (C) 2019 Jari Suominen

//Everyone is permitted to copy and distribute verbatim or modified
//copies of this license document, and changing it is allowed as long
//as the name is changed.
 
//           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

// 0. You just DO WHAT THE FUCK YOU WANT TO.

import gifAnimation.*;
import java.io.*;
import java.awt.image.BufferedImage;

OPC               opc;
float             y = 0;
float             x = 0;
int               ledStrips = 48, ledStripLength = 57;
DisposeHandler    dh;
Gif               animation1, selectedAnimation;

PImage[] frames;
int[] delays;
int framePointer = 0;

void setup()
{  
  //Ruudun koko
  //fullScreen();
  size(400, 400);
  
  if (args == null) {
    println("Please specify gif file.");
    System.exit(1);
  }

  GifDecoder gifDecoder = new GifDecoder();
  InputStream inputStream = createInput(args[0]);
  if (inputStream == null) {
    println("File not found!");
    System.exit(1);
  }
  gifDecoder.read(inputStream);

  // fill up the PImage and the delay arrays
  int n = gifDecoder.getFrameCount();

  frames = new PImage[n];

  for (int i = 0; i < n; i++) {
    BufferedImage frame = gifDecoder.getFrame(i);
    frames[i] = new PImage(frame.getWidth(), frame.getHeight(), ARGB);
    System.arraycopy(frame.getRGB(0, 0, frame.getWidth(), frame
      .getHeight(), null, 0, frame.getWidth()), 0, 
      frames[i].pixels, 0, frame.getWidth() * frame.getHeight());
  }

  n = gifDecoder.getFrameCount();
  delays = new int[n];
  for (int i = 0; i < n; i++) {
    delays[i] = gifDecoder.getDelay(i);
  }
  
  // We will assume all frames to be equal length and set the
  // framerate accordingly
  frameRate(1000/delays[0]);
  
  // Connect to the local instance of fcserver
  opc = new OPC(this, "127.0.0.1", 7890);
  dh = new DisposeHandler(this);

  // Ledinauhojen maaritykset
  // ledStrip (indeksi, nauhan pituus, X koordinantti, Y koordinantti, etaisyys ledien valilla, 

  // SEKTORI 1
  opc.ledStrip(  0, 57, 339.4706, 156.8266, 2, 5.37561, false);
  opc.ledStrip(  60, 57, 345.8924, 194.3954, 2, 5.637409, false);
  opc.ledStrip(  120, 57, 342.3718, 232.3461, 2, 5.899208, false);
  opc.ledStrip(  180, 57, 329.1488, 268.0924, 2, 6.161007, false);
  opc.ledStrip(  240, 57, 307.1246, 299.1984, 2, 6.422806, false);
  opc.ledStrip(  300, 57, 277.7999, 323.5442, 2, 6.684605, false);
  opc.ledStrip(  360, 57, 243.1734, 339.4706, 2, 6.946404, false);
  opc.ledStrip(  420, 57, 205.6046, 345.8924, 2, 7.208203, false);

  opc.ledStrip(  480, 57, 339.4706, 243.1734, 2, 0.907571, false);
  opc.ledStrip(  540, 57, 323.5442, 277.7999, 2, 1.16937, false);
  opc.ledStrip(  600, 57, 299.1984, 307.1246, 2, 1.431169, false);
  opc.ledStrip(  660, 57, 268.0924, 329.1488, 2, 1.692968, false);
  opc.ledStrip(  720, 57, 232.3461, 342.3718, 2, 1.954767, false);
  opc.ledStrip(  780, 57, 194.3954, 345.8924, 2, 2.216566, false);
  opc.ledStrip(  840, 57, 156.8266, 339.4706, 2, 2.478365, false);
  opc.ledStrip(  900, 57, 122.2001, 323.5442, 2, 2.740164, false);


  // SEKTORI 2
  opc.ledStrip(  1920, 57, 167.6539, 57.6282, 2, 5.096355, false);
  opc.ledStrip(  1980, 57, 205.6046, 54.1076, 2, 5.358154, false);
  opc.ledStrip(  2040, 57, 243.1734, 60.5294, 2, 5.619953, false);
  opc.ledStrip(  2100, 57, 277.7999, 76.4558, 2, 5.881752, false);
  opc.ledStrip(  2160, 57, 307.1246, 100.8016, 2, 6.143551, false);
  opc.ledStrip(  2220, 57, 329.1488, 131.9076, 2, 6.40535, false);
  opc.ledStrip(  2280, 57, 342.3718, 167.6539, 2, 6.667149, false);
  opc.ledStrip(  2340, 57, 345.8924, 205.6046, 2, 6.928948, false);

  opc.ledStrip(  2400, 57, 92.8754, 100.8016, 2, 9.564394, false);
  opc.ledStrip(  2460, 57, 122.2001, 76.4558, 2, 9.826193, false);
  opc.ledStrip(  2520, 57, 156.8266, 60.5294, 2, 10.087992, false);
  opc.ledStrip(  2580, 57, 194.3954, 54.1076, 2, 10.349791, false);
  opc.ledStrip(  2640, 57, 232.3461, 57.6282, 2, 10.61159, false);
  opc.ledStrip(  2700, 57, 268.0924, 70.8512, 2, 10.873389, false);
  opc.ledStrip(  2760, 57, 299.1984, 92.8754, 2, 11.135188, false);
  opc.ledStrip(  2820, 57, 323.5442, 122.2001, 2, 11.396987, false);


  // SEKTORI 3
  opc.ledStrip(  1440, 57, 92.8754, 299.1984, 2, 3.001963, false);
  opc.ledStrip(  1500, 57, 70.8512, 268.0924, 2, 3.263762, false);
  opc.ledStrip(  1560, 57, 57.6282, 232.3461, 2, 3.525561, false);
  opc.ledStrip(  1620, 57, 54.1076, 194.3954, 2, 3.78736, false);
  opc.ledStrip(  1680, 57, 60.5294, 156.8266, 2, 4.049159, false);
  opc.ledStrip(  1740, 57, 76.4558, 122.2001, 2, 4.310958, false);
  opc.ledStrip(  1800, 57, 100.8016, 92.8754, 2, 4.572757, false);
  opc.ledStrip(  1860, 57, 131.9076, 70.8512, 2, 4.834556, false);

  opc.ledStrip(  960, 57, 167.6539, 342.3718, 2, 7.470002, false);
  opc.ledStrip(  1020, 57, 131.9076, 329.1488, 2, 7.731801, false);
  opc.ledStrip(  1080, 57, 100.8016, 307.1246, 2, 7.9936, false);
  opc.ledStrip(  1140, 57, 76.4558, 277.7999, 2, 8.255399, false);
  opc.ledStrip(  1200, 57, 60.5294, 243.1734, 2, 8.517198, false);
  opc.ledStrip(  1260, 57, 54.1076, 205.6046, 2, 8.778997, false);
  opc.ledStrip(  1320, 57, 57.6282, 167.6539, 2, 9.040796, false);
  opc.ledStrip(  1380, 57, 70.8512, 131.9076, 2, 9.302595, false);

  //Nauhojen sijainnin näyttö
  opc.showLocations(false);

}


void draw() {
  background(0, 0, 0);
  image(frames[framePointer], 0, 0);
  framePointer++;
  framePointer %= frames.length;
}


public class DisposeHandler {

  DisposeHandler(PApplet pa) {
    pa.registerMethod("dispose", this);
  }

  public void dispose() {
    for (int i =0; i < ledStrips * ledStripLength; i++) {
      opc.setPixel(i, 0);
      opc.writePixels();
    }
  }
}
